package application.model;

import javafx.scene.paint.Color;

public class paintModel {

	private Color color;
	private double brushSize;
	private boolean eraser;

	public paintModel() {
		color = Color.BLACK;
		brushSize = 10;
		eraser = false;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getBrushSize() {
		return brushSize;
	}

	public void setBrushSize(double brushSize) {
		this.brushSize = brushSize;
	}

	public boolean isEraser() {
		return eraser;
	}

	public void toggleEraser() {
		eraser = !eraser;
	}
}
