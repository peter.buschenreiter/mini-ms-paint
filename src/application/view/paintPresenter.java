package application.view;

import application.model.paintModel;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

public class paintPresenter {
	public static final File ROOT = new File("resource");
	private paintModel model;
	private paintView view;

	public paintPresenter(paintModel model, paintView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		addWindowEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getColorPicker().setOnAction(actionEvent -> {
			model.setColor(view.getColorPicker().getValue());
			updateView();
		});

		view.getCanvas().setOnMouseDragged(mouseEvent -> {
			final double x = mouseEvent.getX() - model.getBrushSize() / 2;
			final double y = mouseEvent.getY() - model.getBrushSize() / 2;
			final GraphicsContext gc = view.getCanvas().getGraphicsContext2D();
			gc.setFill(model.getColor());
			if (model.isEraser()) {
				gc.clearRect(x, y, model.getBrushSize(), model.getBrushSize());
			} else {
				gc.fillOval(x, y, model.getBrushSize(), model.getBrushSize());
			}
		});

		view.getSlider().setOnMouseDragged(mouseEvent -> {
			model.setBrushSize(view.getSlider().getValue());
			updateView();
		});

		view.getResetCanvas()
		    .setOnAction(actionEvent -> view.getCanvas()
		                                    .getGraphicsContext2D()
		                                    .clearRect(0, 0, view.getCanvas().getWidth(), view.getCanvas()
		                                                                                      .getHeight()));

		view.getEraser().setOnAction(actionEvent -> {
			model.toggleEraser();
		});

		view.getSave().setOnAction(actionEvent -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("png files", "*.png"));
			fileChooser.setInitialDirectory(ROOT);
			fileChooser.setTitle("Save");
			File file = fileChooser.showSaveDialog(view.getScene().getWindow());
			if (file != null) {
				try {
					WritableImage writableImage = new WritableImage(
							(int) view.getCanvas().getWidth(),
							(int) view.getCanvas().getHeight());
					RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);

					ImageIO.write(renderedImage, "png", file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		view.getLoad().setOnAction(actionEvent -> {

			// FIXME something is wrong since i changed saving writableImage (reversed parameters)
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("png files", "*.png"));
			fileChooser.setInitialDirectory(ROOT);
			fileChooser.setTitle("Load");
			File file = fileChooser.showOpenDialog(view.getScene().getWindow());
			if (file != null) {
				view.getCanvas()
				    .getGraphicsContext2D()
				    .clearRect(0, 0, view.getCanvas().getWidth(), view.getCanvas().getHeight());
				Image img = new Image(file.toURI().toString());
				view.getCanvas()
				    .getGraphicsContext2D()
				    .drawImage(img, 0, 0, view.getCanvas().getWidth(), view.getCanvas().getHeight());
			}
		});
	}

	private void updateView() {
		view.getCurrentColor().setText(model.getColor().toString());
		view.getSlider().setValue(model.getBrushSize());
		view.getColorPicker().setValue(model.getColor());
		view.getBrushSize().setText(String.format("%.1f", model.getBrushSize()));
		view.getEraser().setSelected(model.isEraser());
	}

	private void addWindowEventHandlers() {
		Window window = view.getScene().getWindow();
		// Add event handlers to window
	}
}
