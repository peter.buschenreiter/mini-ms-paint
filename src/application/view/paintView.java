package application.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

public class paintView extends BorderPane {
	private ToolBar toolBar;
	private ColorPicker colorPicker;
	private Canvas canvas;
	private Label currentColor;
	private Slider slider;
	private Label brushSize;
	private Button resetCanvas;
	private CheckBox eraser;
	private MenuItem load;
	private MenuItem save;

	public paintView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		load = new MenuItem("Load");
		save = new MenuItem("Save");
		colorPicker = new ColorPicker();
		Label text = new Label("Current Color: ");
		currentColor = new Label();
		slider = new Slider(1, 50, 10);
		slider.setMajorTickUnit(49);
		slider.setShowTickLabels(true);

		eraser = new CheckBox("Eraser");
		resetCanvas = new Button("Reset");
		brushSize = new Label();
		toolBar = new ToolBar(colorPicker, text, currentColor, slider, brushSize, resetCanvas, eraser);
		canvas = new Canvas();
	}

	private void layoutNodes() {
		canvas.setHeight(480);
		canvas.setWidth(800);
		setCenter(canvas);
		setBottom(toolBar);
		final Menu fileMenu = new Menu("File");
		fileMenu.getItems().add(load);
		fileMenu.getItems().add(save);
		final MenuBar menuBar = new MenuBar(fileMenu);
		setTop(menuBar);
	}

	ColorPicker getColorPicker() {
		return colorPicker;
	}

	Canvas getCanvas() {
		return canvas;
	}

	Label getCurrentColor() {
		return currentColor;
	}

	Slider getSlider() {
		return slider;
	}

	Label getBrushSize() {
		return brushSize;
	}

	Button getResetCanvas() {
		return resetCanvas;
	}

	CheckBox getEraser() {
		return eraser;
	}

	MenuItem getLoad() {
		return load;
	}

	MenuItem getSave() {
		return save;
	}
}
