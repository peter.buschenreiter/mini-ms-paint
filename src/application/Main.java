package application;

import application.model.paintModel;
import application.view.paintPresenter;
import application.view.paintView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		paintModel model = new paintModel();
		paintView view = new paintView();
		stage.setScene(new Scene(view));
		paintPresenter presenter = new paintPresenter(model, view);
		stage.show();

	}
}
